'use strict';
//Empezamos definiendo el módulo principal y añadiendo las dependencias de módulo
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Se fija el modo de localización de HTML5
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
])
//Configuración para los mapas
.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        v: '3.17',
        libraries: 'weather,geometry,visualization',
        language: 'es'
    });
})
//Constantes
.run(function ($rootScope) {	
	$rootScope.HOSTNAME = window.location.origin;
});

//Definimos la función que inicia la aplicación
angular.element(document).ready(function() {
	//Fix de la redirección de Facebook
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Iniciamos la aplicación
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});