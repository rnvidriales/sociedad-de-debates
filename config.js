'use strict';
// Iniciamos la configuración del módulo de la aplicación AngularJS
var ApplicationConfiguration = (function() {
	// Iniciamos el módulo de opciones configuración
	var applicationModuleName = 'sociedadDebatesUAM';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  
	                                           'ui.router', 'ui.bootstrap', 'ui.utils', 'uiGmapgoogle-maps', 'youtube-embed', 'vimeoEmbed',
                                               'smoothScroll','ngScrollSpy'];

	// Añadimos un nuevo módulo de manera vertical
	var registerModule = function(moduleName, dependencies) {
		// Creamos un nuevo módulo de angular
		angular.module(moduleName, dependencies || [])
		.constant('HOSTNAME', window.location.origin);

		// Añadimos el módulo al fichero de configuración de angular
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();