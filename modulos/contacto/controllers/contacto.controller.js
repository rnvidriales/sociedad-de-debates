'use strict';

angular.module('contacto').controller('ContactoController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {        
       //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'contacto',
            //Título del módulo
            titulo : 'Contacto',
            //Posición en el menú
            posicion : 7
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);        
	}
]);