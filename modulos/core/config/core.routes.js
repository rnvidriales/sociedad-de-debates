'use strict';

// Fijamos las rutas principales
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redireccionamos al índice si la ruta no se encuentra
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modulos/core/views/home.view.html'
		});
	}
]);