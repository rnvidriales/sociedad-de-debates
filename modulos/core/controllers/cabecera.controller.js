'use strict';

angular.module('core').controller('CabeceraController', ['$scope', '$filter', '$location','$anchorScroll', 'Menu',
	function($scope,$filter,$location,$anchorScroll,Menu) { 
       $scope.scrollTo = function(id) {
          $location.hash(id);
          $anchorScroll();
       }
       
        $scope.cabeceraFondo = false;
        
        //Lo pasamos a las vistas        
        var actualizarMenu = function(){
            $scope.menu = Menu.obtenerMenu();
            $scope.menu = $filter('orderBy')($scope.menu,'posicion');
        };
        Menu.registerObserverCallback(actualizarMenu);
        
        //Activamos el cambio de clase por scroll
        $scope.cbpAnimatedHeader = {
            docElem : document.documentElement,
            header : document.querySelector( '.navbar-default' ),
            didScroll : false,
            changeHeaderOn : 200,
            init : function () {
                window.addEventListener( 'scroll', function( event ) {
                    if( !$scope.cbpAnimatedHeader.didScroll ) {
                        $scope.cbpAnimatedHeader.didScroll = true;
                        setTimeout( $scope.cbpAnimatedHeader.scrollPage, 250 );
                    }
                }, false );
            },
            scrollPage : function () {
                var sy = $scope.cbpAnimatedHeader.scrollY();
                if ( sy >= $scope.cbpAnimatedHeader.changeHeaderOn ) {
                    $scope.cabeceraFondo = true;
                }
                else {
                    $scope.cabeceraFondo = false;
                }
                $scope.cbpAnimatedHeader.didScroll = false;
                $scope.$apply();
            },
            scrollY : function () {
                return window.pageYOffset || $scope.cbpAnimatedHeader.docElem.scrollTop;
            }
        };
        
        $scope.cbpAnimatedHeader.init();        
	}
]);