angular.module('core')
.directive('wrapOwlcarousel', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            scope.$watch('elementos', function (newval, oldval) {
                var options = scope.$eval($(element).attr('data-options'));
                $(element).owlCarousel(options);
            });
        }
    };
});