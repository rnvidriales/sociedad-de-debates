'use strict';

//Servicio Menu usado para gestionar los menús
angular.module('core').service('Menu', ['$window',
    function($window) {
        var observerCallbacks = [];
        
        //Registrar un observador de cambios
        this.registerObserverCallback = function(callback){
            observerCallbacks.push(callback);
        };

        //Se llama cada vez que hay que notificar un cambio a los implicados
        var notifyObservers = function(){
                angular.forEach(observerCallbacks, function(callback){
                callback();
            });
        };
        
		// Definimos un objeto menu
		this.menu = {};

		//Comprobamos que el menú existe
		this.validarExistenciaMenu = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menu[menuId]) {
					return true;
				} else {
					throw new Error('Menu no existe');
				}
			} else {
				throw new Error('MenuId no ha sido encontrado');
			}

			return false;
		};
        
		// Obtenemos el objeto del menu a través de su identificador
		this.obtenerMenu = function() {
			return $window._.values(this.menu);
		};
        
		// Obtenemos el objeto del menu a través de su identificador
		this.obtenerMenuItem = function(menuId) {
			// Comprobamos que el menú existe
			this.validarExistenciaMenu(menuId);

			// Devolvemos el objeto de menu
			return this.menu[menuId];
		};

		// Añadimos un objeto de menu por id
		this.addMenuItem = function(menuId, menuTitulo, posicion) {
			// Creamos el nuevo menu
			this.menu[menuId] = {
				titulo: menuTitulo,
				link: menuId,
                posicion: posicion
			};
            
            notifyObservers();
            
			// Devolvemos el objeto de menu
			return this.menu[menuId];
		};

		// Eliminamos un objeto de menu por id
		this.eliminarMenuItem = function(menuId) {
			// Comprobamos que el menú existe
			this.validarExistenciaMenu(menuId);

			delete this.menu[menuId];
            
            notifyObservers();
		};
	}
]);