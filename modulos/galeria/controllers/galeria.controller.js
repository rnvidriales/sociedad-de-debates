'use strict';

angular.module('galeria').controller('GaleriaController', ['$scope', '$window', 'Menu',
 function ($scope, $window, Menu) {
        //Elemento del menú
        $scope = $window._.assign($scope, {
            //Indentificador del módulo
            id_modulo: 'galeria',
            //Título del módulo
            titulo: 'Galería',
            //Posición en el menú
            posicion: 3
        });

        //Añadimos el elemento al manú   
        Menu.addMenuItem($scope.id_modulo, $scope.titulo, $scope.posicion);

        $scope.elementos = [
            {
                imagen: [
                    '/img/galeria/43.jpg',
                    '/img/galeria/44.jpg',
                    '/img/galeria/45.jpg',
                    '/img/galeria/46.jpg',
                    '/img/galeria/47.jpg',
                    '/img/galeria/48.jpg',
                    '/img/galeria/49.jpg'
                ],
                titulo: 'I Torneo Interpoli de la Universidad Politécnica de Valencia',
                subtitulo: 'Moción: ¿Debería implementarse una renta básica universal en España?',
                texto: ''.concat(
                    '<p> Al inicio del curso 2014/2015 tres de nuestros debatientes viajaron a Valencia en representación de la Sociedad de Debates UAM a tratar un tema de actualidad, a la par que controvertido, como es el de la renta básica universal.</p>' )
            },
            {
                imagen: [
                    '/img/galeria/50.jpg',
                    '/img/galeria/51.jpg',
                    '/img/galeria/52.jpg',
                    '/img/galeria/53.jpg',
                    '/img/galeria/54.jpg',
                    '/img/galeria/55.jpg',
                    '/img/galeria/56.jpg',
                    '/img/galeria/57.jpg',
                    '/img/galeria/58.jpg',
                    '/img/galeria/59.jpg',
                    '/img/galeria/60.jpg',
                    '/img/galeria/61.jpg',
                    '/img/galeria/62.jpg'
                ],
                titulo: 'Torneo de debate académico Séneca de la Universidad de Córdoba',
                subtitulo: 'Moción:  ¿Fue culpable el pueblo alemán del genocidio judío durante la Segunda Guerra mundial?',
                texto: ''.concat(
                    '<p>Un equipo de nuestra Sociedad se desplazó a la Universidad de Córdoba donde obtuvieron grandes resultados fruto del esfuerzo y la constancia. Nuestros debatientes fueron semifinalistas y nuestra presidenta Irene Miguelsanz Villanueva obtuvo el premio a mejor oradora. ¡Enhorabuena!</p>' )
            },
            {
                imagen: [
                    '/img/galeria/2.jpg',
                    '/img/galeria/1.jpg',
                    '/img/galeria/3.jpg',
                    '/img/galeria/4.jpg',
                    '/img/galeria/18.jpg',
                    '/img/galeria/19.jpg',
                    '/img/galeria/20.jpg',
                    '/img/galeria/21.jpg',
                    '/img/galeria/22.jpg'
                ],
                titulo: 'Debate de exhibición en la Facultad de Derecho',
                subtitulo: 'Moción: ¿Deberían mantenerla los países que la tienen su capacidad de veto en la ONU?',
                texto: ''.concat(
                    '<p>Este fue el primero de muchos debates organizados por la Sociedad de Debates de la UAM. Tuvo ',
                    'una gran acogida, lo cual nos motivó para continuar mostrando nuestra actividad dentro de nuestra Universidad.</p>' )
            },
            {
                imagen: [
                    '/img/galeria/6.jpg',
                    '/img/galeria/7.jpg',
                    '/img/galeria/11.jpg',
                    '/img/galeria/14.jpg' ,
                    '/img/galeria/23.jpg' ,
                    '/img/galeria/24.jpg' ,
                    '/img/galeria/25.jpg' ,
                    '/img/galeria/26.jpg' ,
                    '/img/galeria/27.jpg'                   
                ],
                titulo: 'Debate de exhibición en la facultad de filosofía',
                subtitulo: 'Moción: ¿Fue el pueblo alemán culpable del genocidio judío durante la Segunda Guerra Mundial?',
                texto: ''.concat(
                    '<p>Ante un tema tan controvertido, nuestros debatientes estuvieron a la altura y nos brindaron un gran debate de exhibición.</p>'
                )
            },
            {
                imagen: [
                    '/img/galeria/9.jpg',
                    '/img/galeria/5.jpg',
                    '/img/galeria/28.jpg',
                    '/img/galeria/29.jpg',
                    '/img/galeria/30.jpg'
                ],
                titulo: 'Torneo de Debate-Oratoria Jóvenes Abogados en Movimiento',
                subtitulo: 'Moción: Prisión permanente revisable',
                texto: ''.concat(
                    '<p>El martes 9 de diciembre de 2014 varios miembros de nuestra Sociedad acudieron al torneo mencionado. ',
                    '¡Enhorabuena a nuestra presidenta Irene Miguelsanz Villanueva por haber obtenido el segundo premio!</p>'
                )
            },
            {
                imagen: [
                    '/img/galeria/36.jpg',
                    '/img/galeria/37.jpg',
                    '/img/galeria/38.jpg',
                    '/img/galeria/39.jpg',
                    '/img/galeria/40.jpg',
                    '/img/galeria/41.jpg',
                    '/img/galeria/42.jpg'
                ],
                titulo: 'Debate de exhibición y coloquio en la facultad de Derecho.',
                subtitulo: 'Moción: ¿Debería legalizarse la venta y el consumo de marihuana?',
                texto: ''.concat(
                    '<p>En esta ocasión, la Sociedad de Debates UAM participó de forma conjunta con la revista Ágora llevando a cabo un gran debate el cual dio mucho que pensar. Posteriormente se realizó un interesante coloquio acerca de la moción.</p>'
                )
            },
            {
                imagen: [
                    '/img/galeria/10.jpg',
                    '/img/galeria/12.jpg',
                    '/img/galeria/15.jpg',
                    '/img/galeria/16.jpg',
                    '/img/galeria/17.jpg',
                    '/img/galeria/31.jpg',
                    '/img/galeria/32.jpg',
                    '/img/galeria/33.jpg',
                    '/img/galeria/34.jpg',
                    '/img/galeria/35.jpg'
                ],
                titulo: 'I Torneo Nacional de debate CDU en Córdoba',
                subtitulo: 'Moción: ¿Cumplen actualmente los medios de comunicación con su función en las sociedades democráticas?',
                texto: ''.concat(
                    '<p>Dos equipos de Sociedad de Debates UAM se desplazaron a Córdoba en febrero en representación de nuestra ',
                    'Universidad, los cuales llevaron a cabo una gran actuación.</p>'
                )
            }
        ];

        $scope.videos = [
            {
                url: 'http://youtu.be/igL4-Ni_U4o',
                titulo: 'Presentación Sociedad de Debates',
                subtitulo: 'Cuál es nuestro objetivo y nuestro proyecto'
            },
            {
                url: 'https://vimeo.com/149444132',
                titulo: 'Debate judicial en Córdoba',
                subtitulo: null

            },
            {
                url: 'https://www.youtube.com/watch?v=qBrx4turzCk',
                titulo: 'Entrevista El Confidencial Digital',
                subtitulo: 'Conoce más nuestra actividad cotidiana'

            },
            {
                url: 'https://www.youtube.com/watch?v=4zA8RmoyZf0',
                titulo: 'XII Premio al emprendedor universitario (CIADE UAM)',
                subtitulo: null

            }
        ];
 }
]);