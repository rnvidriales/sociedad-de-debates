'use strict';

angular.module('info').controller('InfoController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {        
       //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'info',
            //Título del módulo
            titulo : 'Información',
            //Posición en el menú
            posicion : 1
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);
        
        //Contenidos
        $scope.elementos = [
            {
                icono : 'fa-info', //ver iconos en http://fortawesome.github.io/Font-Awesome/icons/
                titulo : 'El proyecto',
                texto : 'La Sociedad de Debates de la Universidad Autónoma de Madrid es un proyecto que comprende la <strong>formación en habilidades de oratoria, argumentación y debate</strong>, y su posterior puesta en práctica en debates de competición.'
            },
            {
                icono : 'fa-university', 
                titulo : 'Contexto',
                texto : 'Cada vez son más las universidades nacionales e internacionales, ya sean públicas o privadas, que han decidido emprender un proyecto similar a éste con el objetivo de <strong>ofrecer a sus alumnos una formación complementaria</strong> a la teórica de especial valor.'
            },
            {
                icono : 'fa-question', 
                titulo : 'Motivo',
                texto : '<strong>La presencia de una Sociedad de Debate es fundamental para cualquier universidad</strong> debido a las habilidades que gracias al debate se desarrollan: pensamiento crítico, trabajo en equipo, reflexión, conocimiento profundo de los temas, etc.'
            },
            {
                icono : 'fa-heart', 
                titulo : 'Oportunidad',
                texto : 'Es una gran oportunidad para los estudiantes participantes, que <strong>perfeccionarán habilidades fundamentales para su futuro</strong>. En definitiva, queremos ensalzar la importancia de estas destrezas tan a menudo olvidadas, o apartadas a un segundo plano, que deben ser apoyadas por toda la excelencia académica.'
            },
            {
                icono : 'fa-magic', 
                titulo : 'Método',
                texto : 'Se ofrecerán periódicamente <strong>cursos</strong> que formen en habilidades comunicativas, se organizarán <strong>debates</strong> y mesas redondas que fomenten el espíritu crítico, y se vertebrará la <strong>participación</strong> de los socios en distintos torneos de oratoria y debate.'
            },
            {
                icono : 'fa-users', 
                titulo : 'Objetivos',
                texto : 'A través de las actividades previstas quieren alcanzarse los siguientes objetivos:	'+
                   ' <ul class="info-objetivos">'+
                       ' <li class="text-muted">'+
                         '   <span class="glyphicon glyphicon-ok"></span> Perder el miedo a comunicar ante una '+
                        '    audiencia</li>'+
                       ' <li class="text-muted">'+
                    '        <span class="glyphicon glyphicon-ok"></span> Mejorar las destrezas de argumentación '+
                    '        oral y de persuasión</li>'+
                    '    <li class="text-muted">'+
                     '       <span class="glyphicon glyphicon-ok"></span> Conocer los fundamentos y la estructura '+
                     '       del debate de competición</li>'+
                     '   <li class="text-muted">'+
                     '       <span class="glyphicon glyphicon-ok"></span> Trabajar en equipo para conformar una '+
                    '        estrategia común</li>'+
                     '   <li class="text-muted">'+
                     '       <span class="glyphicon glyphicon-ok"></span> Acudir a torneos de debate, nacionales e '+
                    '        internacionales en representación de la Universidad Autónoma de Madrid</li>'+
                    '</ul>'
            }            
        ];
        
	}
]);