'use strict';

angular.module('miembros').controller('MiembrosController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {  
        //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'miembros',
            //Título del módulo
            titulo : 'Miembros',
            //Posición en el menú
            posicion : 4
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);  
        
        $scope.isMasmiembros = true;
        
        $scope.miembros = [
            {
                imagen : '/img/miembros/irene.jpg', 
                nombre : 'Irene Miguelsanz Villanueva',
                cargo : 'Directora',
                estudios : 'Doble grado en Derecho y Ciencias Políticas y de la Administración Pública'
            },
            {
                imagen : '/img/miembros/alicia.jpg', 
                nombre : 'Alicia Maddio Medina',
                cargo : 'Presidenta',
                estudios : 'Estudiante de 3º curso de doble grado en Derecho y Ciencias Políticas'
            },
            {
                imagen : '/img/miembros/juncal.jpg', 
                nombre : 'Juncal León Ruz',
                cargo : 'Vicepresidenta',
                estudios : 'Estudiante de 2º curso de doble grado en Derecho y Ciencias Políticas y de la Administración Pública'
            },
            {
                imagen : '/img/miembros/miriam.jpg', 
                nombre : 'Miriam Ferradanes Sampedro',
                cargo : 'Tesorera',
                estudios : 'Estudiante de 4º curso del doble grado en Derecho y Administración y Dirección de Empresas'
            },
            {
                imagen : '/img/miembros/roberto.jpg', 
                nombre : 'Roberto Naharro Vidriales',
                cargo : 'Secretario',
                estudios : 'Ingeniero en Informática'
            },
            {
                imagen : '/img/miembros/javi.jpg', 
                nombre : 'Javier Prieto Prieto',
                cargo : 'Vocal',
                estudios : 'Estudiante de 4º curso de grado en Física y 4º curso de grado en Matemáticas'
            },
            {
                imagen : '/img/miembros/carlos.jpg', 
                nombre : 'Carlos Pérez Alcázar',
                cargo : 'Vocal',
                estudios : 'Estudiante de 2º curso de grado en Filosofía'
            },
            {
                imagen : '/img/miembros/guillermo.jpg', 
                nombre : 'Guillermo Serrano Fernández',
                cargo : 'Vocal',
                estudios : 'Estudiante de 4º curso del doble grado en Derecho y Administración y Dirección de Empresas'
            }
        ];
        
        $scope.masMiembros = [
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Kay Garland',
                cargo : 'Lead Designer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Larry Parker',
                cargo : 'Lead Marketer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            },
            {
                imagen : 'http://placehold.it/225x225', 
                nombre : 'Diana Pertersen',
                cargo : 'Lead Developer'
            }
        ];
	}
]);