'use strict';

angular.module('participa').controller('ParticipaController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {        
       //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'participa',
            //Título del módulo
            titulo : '¡Participa!',
            //Posición en el menú
            posicion : 6
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);      
        
        $scope.isCartel = true;
	}
]);