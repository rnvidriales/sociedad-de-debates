'use strict';

angular.module('torneos').controller('TorneosController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {        
       //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'torneos',
            //Título del módulo
            titulo : 'Actividades',
            //Posición en el menú
            posicion : 5
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);
    }
]);