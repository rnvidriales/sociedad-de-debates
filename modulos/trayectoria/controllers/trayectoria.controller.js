'use strict';

angular.module('trayectoria').controller('TrayectoriaController', ['$scope', '$window', 'Menu',
	function($scope,$window,Menu) {  
        //Elemento del menú
        $scope = $window._.assign($scope,{
            //Indentificador del módulo
            id_modulo : 'trayectoria',
            //Título del módulo
            titulo : 'Trayectoria',
            //Posición en el menú
            posicion : 2
        });
        
        //Añadimos el elemento al manú   
		Menu.addMenuItem($scope.id_modulo,$scope.titulo,$scope.posicion);  
        
        $scope.isMasTrayectoria = true;
        
        $scope.elementos = [
            {
                imagen : '/img/trayectoria/1.jpg', 
                fecha : 'Abril de 2013',
                titulo: 'Fundación de la Sociedad de Debates de la UAM',
                texto : 'Comprobamos que la universidad nos enseñaba mucha teoría sin enseñarnos, en cambio, a aplicarla en la práctica; no sabíamos llevar a cabo una presentación adecuada ni una ponencia ante un público numeroso y tampoco sabíamos expresar de forma estructurada y convincente nuestras ideas por lo que decidimos crear la Sociedad de Debates para poner fin a estos problemas, para desarrollar nuestras habilidades de comunicación y hacer resaltar en nuestra Universidad la importancia de las mismas.'
            },
            {
                imagen : '/img/trayectoria/2.jpg', 
                fecha : 'Julio de 2014',
                titulo: 'Subcampeones y décimo puesto en el top ten de mejores oradores del CMUDE',
                texto : 'Representando a la Universidad Autónoma de Madrid, el equipo liderado por Irene Miguelsanz Villanueva consiguió la plaza de Subcampeones del Campeonato Mundial Universitario de Debate en Español, además del puesto número décimo, logrando dicha estudiante entrar en el top ten de mejores oradores de entre los ciento sesenta participantes, siendo la única mujer en dicho ranking y en la final que se disputó entre los equipos de ICADE, la Universidad de Córdoba y la Universidad del Rosario de Bogotá, alzándose esta última con el primer premio'
            },
            {
                imagen : '/img/trayectoria/3.jpg', 
                fecha : 'Octubre de 2014',
                titulo: 'Ganadores del I Torneo de Debate Escrito de la AGADE',
                texto : 'El equipo de la Sociedad de Debates de la Universidad Autónoma de Madrid resultó vencedor de este Torneo, en el cual participaron dieciséis equipos procedentes de España, Perú y Colombia durante todo un mes, desde el 14 de septiembre hasta el 12 de octubre, de manera online. El equipo ganador estaba formado por Elías Manzano Corona, Mar López Salmerón, Guillermo Serrano Fernández e Irene Miguelsanz Villanueva.'
            },
            {
                imagen : '/img/trayectoria/4.jpg', 
                fecha : 'Noviembre de 2014',
                titulo: 'Mejor Orador Torneo de Debate Académico "Séneca" de la Universidad de Córdoba',
                texto : 'La presidenta de la Sociedad de Debates, Irene Miguelsanz Villanueva, obtuvo el premio a mejor orador de entre 80 debatientes procedentes de 9 Universidades distintas de toda España, obteniendo el equipo el puesto de semifinalistas, entre los 4 primeros equipos de 21'
            },
            {
                imagen : '/img/trayectoria/5.jpg', 
                fecha : 'Noviembre de 2014',
                titulo: 'Ganadores del II Torneo de Debate Académico del Colegio Mayor Isabel de España',
                texto : 'El equipo de la Sociedad de Debates de la Universidad Autónoma de Madrid, formado por Bernardino León Reyes, Guillermo Serrano fernández e Irene Miguelsanz Villanueva, resultó vencedor de este Torneo, en el cual participaron veinte equipos procedentes de Universidades prestigiosas de toda España'
            },
            {
                imagen : '/img/trayectoria/6.jpg', 
                fecha : 'Diciembre de 2014',
                titulo: 'II Premio Torneo de Debate-Oratoria Jóvenes Abogados en Movimiento',
                texto : 'El martes 9 de diciembre varios miembros de la Sociedad de Debates acudieron al I Torneo de Debate-Oratoria organizado por la Asociación del Colegio de Abogados Jóvenes Abogados en Movimiento (JAM), obteniendo el segundo premio nuestra presidenta Irene Miguelsanz Villanueva'
            },
            {
                imagen : '/img/trayectoria/7.jpg', 
                fecha : 'Diciembre de 2014',
                titulo: 'Ganadores del Accésit de Humanidades y Ciencias Sociales del XII Premio al Emprendedor Universitario del CIADE',
                texto : 'A lo largo de 8 meses, el proyecto de la Sociedad de Debate superó 3 fases eliminatorias hasta llegar a la final en la cual obtuvo dicho premio'
            },
            {
                imagen : '/img/trayectoria/8.jpg', 
                fecha : 'Julio de 2015',
                titulo: 'Subcampeones y tercer puesto en el top ten de mejores oradores del CMUDE',
                texto : 'Representando a la Universidad Autónoma de Madrid, el equipo formado por Irene Miguelsanz Villanueva y Mariella De la Cruz Taboada consiguió la plaza de Subcampeones del Campeonato Mundial Universitario de Debate en Español (revalidando el título ya conseguido en 2014) de entre ciento ocho equipo; además, Irene logró el tercer puesto en el top ten de mejores oradores de entre doscientos dieciséis debatientes. '
            },
            {
                imagen : '/img/trayectoria/9.jpg', 
                fecha : 'Diciembre de 2015',
                titulo: 'Ganadores Pasarela Judicial en Córdoba de la Liga de Debate Judicial',
                texto : 'El Consejo General de Debate Judicial, un año más, organiza la Liga de Debate Judicial. Esta Liga está dirigida a estudiantes de Derecho de toda la geografía española con el fin de completar su formación en el mundo judicial. Los días 11 y 12 de diciembre tuvo lugar esta Liga en Córdoba, donde el equipo de la Universidad Autónoma de Madrid formado por Juncal León Ruz, Guillermo Serrano Fernández e Irene Miguelsanz Villanueva resultó vencedor del evento.'
            }
        ];
        
        $scope.masTrayectoria = [
            {
                anyo : 2013, 
                eventos : [
                    'Participación en la V Liga de Debate UC3M', 
                    'Participación en el III Campeonato Mundial Universitario de Debate en Español',
                    'Participación en el I Torneo de Debate Colegio Mayor Isabel de España', 
                    'Participación en el II Torneo de Debate Académico Séneca',
                    'Participación en el I Concurso de Oratoria Jurídica de la Universidad Autónoma de Madrid'
                ]                 
            },            
            {
                anyo : 2014, 
                eventos : [
                    'Participación en el II Torneo Nacional de Debate', 
                    'Participación en el IV Torneo de Debate Interuniversitario Sociedad de Debate Complutense-Instituto',
                    'Participación en la VI Liga de Debate UC3M', 
                    'Participación en el I Torneo Interuniversitario Universidad Politécnica de Valencia, UPV'
                ]                 
            },            
            {
                anyo : 2015, 
                eventos : [
                    'Participación I Torneo de Debate Académico del Club de Debate Universitario de Córdoba',
                    'Participación en el V Torneo de Debate Interuniversitario Sociedad de Debate Complutense-Instituto',
                    'Participacion en el I Torneo de Colegios Mayores de la Comunidad de Madrid, en representación del Colegio Mayor Juan Luis Vives de la Universidad Autónoma de Madrid',
                    'Participación I Torneo de Parlamentario Británico de la Universidad de Córdoba-Europe Direct',
                    'Participación en el X Torneo de la Universidad Francisco de Vitoria',
                    'Participación en el V Torneo Tres Culturas, organizado por la Asociación Juvenil de Debate Dilema',
                    'Participación en el IV Oxford Women Open',
                    'Participación en el I Lisbon Open de la Universidad de Lisboa',
                    'Participación en el Torneo Universitario de Debate Escrito de la Universidad de Chile (TUDEUCH)',
                    'Participación en torneo Pasarela Judicial en Salamanca de la Liga de Debate Judicial organizada por el Consejo General de Debate Judicial',
                    'Participación en el III Torneo de Debate Colegio Mayor Isabel de España',
                    'Participación en el IV Torneo de Debate Académico Séneca de la Universidad de Córdoba',
                    'Participación en el I Torneo de Parlamentario Británico de la Universidad Rey Juan Carlos'
                ]                 
            }
        ];
	}
]);